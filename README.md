arima-garch-fitting
===

Automated fitting of ARIMA and GARCH model on real data.

Dependencies
---

- numpy

- matplotlib

- pandas

- statsmodels

- arch

Example output
---

![Example](https://gitlab.com/arkadiuszhryc/ArimaGarchFitting/raw/master/preview.png "Example")