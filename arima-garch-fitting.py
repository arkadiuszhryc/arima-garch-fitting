import os
import urllib.request
import math
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

from statsmodels.tsa.arima_model import ARIMA
from arch import arch_model

#We must download data for analysis
print('Welcome in ARIMA-GARCH analysis.')
ticker = input('Enter ticker: ').lower()
url = 'https://stooq.pl/q/d/l/?s=' + ticker + '&i=d'  
urllib.request.urlretrieve(url, 'notowania.csv')
stooq_data = pd.read_csv('notowania.csv')
os.remove("notowania.csv")

#We must reorganize data, because we will use only close prices in analysis
stooq_data.columns = ['Date', 'Open', 'Maximal', 'Minimal', 'Close', 'Volume']
stooq_data['Returns'] = np.log(stooq_data['Close']) - np.log(stooq_data['Close'].shift(1))
stooq_data = stooq_data[1:]
stooq_data_learn = stooq_data['Returns']
date_range = pd.date_range(stooq_data.index[0], periods = len(stooq_data), freq = 'D')
stooq_data_learn.index = date_range
learn_data = pd.Series(stooq_data_learn, index = date_range)
stooq_data['Date'] = pd.to_datetime(stooq_data["Date"])
stooq_data.index = stooq_data["Date"]
stooq_data_price = pd.Series(data = stooq_data['Close'], index = stooq_data['Date'])
stooq_data_return = pd.Series(data = stooq_data['Returns'], index = stooq_data['Date'])

#We will try fit best ARIMA model
prev_fit = math.inf

for x in range(5):
    for y in range(5):
        model = ARIMA(learn_data, order = (x,0,y))
        try:
            model_fit = model.fit(disp = 0)
            if model_fit.aic < prev_fit:
                p = x
                q = y
                prev_fit = model_fit.aic
        except:
            continue
            
ar = p
ma = q

#We will try fit best GARCH model
for x in range(3):
    for y in range(3):
        for z in range (2):
            if x == 0:
                model = arch_model(learn_data, p = x, o = 1, q = y)
            else:
                model = arch_model(learn_data, p = x, o = 0, q = y)
            try:
                model_fit = model.fit(update_freq = 10)
                if model_fit.aic < prev_fit:
                    p = x
                    q = y
                    prev_fit = model_fit.aic
            except:
                continue

arch = p
garch = q

#Final output
print('Preview of the data:')
fig = plt.figure(num = 1, figsize=(8, 10))
plt.subplot(211)
stooq_data_price.plot()
plt.title(ticker.upper() + ' - price')
plt.ylabel('Price')

plt.subplot(212)
stooq_data_return.plot()
plt.title(ticker.upper() + ' - returns')

fig.tight_layout()

plt.show()

print('Best fitting models:')
print('- mean model: ARMA(' + str(ar) + ',' + str(ma) + ')')
print('- volatility model: GARCH(' + str(arch) + ',' + str(garch) + ')')
